module StatusFlow
  module FormHelper
    def publication_stages
      StatusFlow::Stage.stages.values
    end

    def visibilities
      StatusFlow::Visibility.visibilities.values
    end
  end
end

class CreateNonstandardFieldsParents < ActiveRecord::Migration[5.0]
  def change
    create_table :nonstandard_fields_parents do |t|
      t.integer :pub_status
      t.integer :visible

      t.timestamps
    end
  end
end

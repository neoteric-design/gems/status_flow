class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  include StatusFlow::ActiveRecord
end

require 'rails_helper'

module StatusFlow
  RSpec.describe ActiveRecord do
    let(:parent) { Parent.new }
    let(:nonstandard_parent) { NonstandardFieldsParent.new }

    describe 'setting options' do
      let(:default_options) { { status_field: :publication_status,
                                visibility_field: :visibility } }
      # from dummy app model
      let(:custom_options) { { status_field: :pub_status,
                               visibility_field: :visible } }

      context 'options unset' do
        it 'has defaults' do
          expect(parent.class.status_flow_options).to eq(default_options)
        end
      end

      context 'options set' do
        it 'has what user set' do
          expect(nonstandard_parent.class.status_flow_options).to eq(custom_options)
        end
      end
    end

    describe 'setting up enums' do
      pending
    end
  end
end

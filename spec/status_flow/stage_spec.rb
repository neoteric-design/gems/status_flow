require 'rails_helper'

module StatusFlow
  RSpec.describe Stage do
    let(:stage) { Stage.new('test', 1) }

    it { expect(stage).to respond_to(:name, :index) }

    describe 'registering' do
      it 'instance can' do
        stage.register
        expect(Stage.stages[stage.name]).to eq(stage)
      end

      it 'can produce a hash suitable for Rails enum method' do
        Stage.reset!
        Stage.new('one', 1).register
        Stage.new('two', 2).register

        expect(Stage.stages_for_enum).to include({'one' => 1, 'two' => 2})
      end
    end
  end
end

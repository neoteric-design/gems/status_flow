module StatusFlow
  module ActiveRecord
    def self.included(klass)
      klass.extend ClassMethods
      klass.include InstanceMethods
    end

    module ClassMethods
      def status_flow(status_field: :publication_status,
                      visibility_field: :visibility)
        class_attribute :status_flow_options
        self.status_flow_options = { status_field: status_field,
                                     visibility_field: visibility_field }
        enum status_field => StatusFlow::Stage.stages_for_enum
        enum visibility_field => StatusFlow::Visiblity.visibilities_for_enum
      end
    end

    module InstanceMethods
    end
  end
end
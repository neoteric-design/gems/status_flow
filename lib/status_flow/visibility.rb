module StatusFlow
  class Visibility
    class_attribute :visibilities
    attr_reader :name, :index, :label

    def initialize(name, index, label: nil)
      @name = name
      @label = label || name.to_s.titleize
      @index = index
    end

    def register
      self.class.register self
    end

    class << self
      def register(visibility)
        visibilities[stage.name] = visibility
      end

      def visibilities_for_enum
        Hash[visibilities.values.map { |s| [s.name, s.index] }]
      end

      def reset!
        self.visibilities = HashWithIndifferentAccess.new
        register Visibility.new('hidden', 0)
        register Visibility.new('visible', 1)
      end

      def find(id)
        visibilities[id]
      end
    end
    self.reset!
  end
end

module StatusFlow
  class Stage
    class_attribute :stages
    attr_reader :name, :index, :label

    def initialize(name, index, label: nil)
      @name = name
      @label = label || name.to_s.titleize
      @index = index
    end

    def register
      self.class.register self
    end

    class << self
      def register(stage)
        stages[stage.name] = stage
      end

      def stages_for_enum
        Hash[stages.values.map { |s| [s.name, s.index] }]
      end

      def reset!
        self.stages = HashWithIndifferentAccess.new
        register Stage.new('drafted', 0)
        register Stage.new('published', 1)
      end

      def find(id)
        stages[id]
      end
    end
    self.reset!
  end
end

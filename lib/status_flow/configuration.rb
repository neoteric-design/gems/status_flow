module StatusFlow
  class << self
    attr_reader :config

    def configure
      @config = Configuration.new
      yield config
    end
  end

  class Configuration
    def initialize
    end

    def register_stage(*args)
      Stage.new(*args).register
    end
  end
end

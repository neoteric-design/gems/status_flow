$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "status_flow/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "status_flow"
  s.version     = StatusFlow::VERSION
  s.authors     = ["Madeline Cowie"]
  s.email       = ["madeline@cowie.me"]
  s.homepage    = "http://www.neotericdesign.com"
  s.summary     = "Workflow status of CMS object"
  s.description = "Workflow status of CMS object"
  s.license     = "Closed"

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.0"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
end

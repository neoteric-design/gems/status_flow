# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# StatusFlow
Publishing workflow stages for CMS objects.

StatusFlow manages two related aspects of CMS models, publishing status and visibility.

Add drafted, published and any state in between.

Also provides a visibility setting


## Usage

By default it assumes you have integer columns of `publication_status` and `visibility`. But you can configure these when you set up StatusFlow in your model.


```
class Page
  # Defaults
  status_flow

  # or
  status_flow status_field: :some_field, visibility_field: :some_other_field
end
```

StatusFlow leverages [ActiveRecord::Enum](http://api.rubyonrails.org/files/activerecord/lib/active_record/enum_rb.html) to handle stages. This means for each stage you'll get all the methods and scopes it provides, like `Page.published` and `@page.published?`


### Publishing stages

StatusFlow starts off with Drafted and Published stages, and lets you add your own. Label is optional, if none given a titleized name will be used.

```config/initializers/status_flow.rb

StatusFlow.configure do |config|
  config.register_stage :review, 2, label: 'Pending Review'
  config.register_stage :copy_ready, 3
end
```


### Visibility


## Installation
Add this line to your application's Gemfile:

```ruby
gem 'status_flow'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install status_flow
```
